dev-kitA Personal tool to setup a Dev environment

```
        ▄▄                                       ▄▄
      ▀███                           ▀███        ██   ██
        ██                             ██             ██
   ▄█▀▀███   ▄▄█▀██▀██▀   ▀██▀         ██  ▄██▀▀███ ██████
 ▄██    ██  ▄█▀   ██ ██   ▄█           ██ ▄█     ██   ██
 ███    ██  ██▀▀▀▀▀▀  ██ ▄█     █████  ██▄██     ██   ██
 ▀██    ██  ██▄    ▄   ███             ██ ▀██▄   ██   ██
  ▀████▀███▄ ▀█████▀    █            ▄████▄ ██▄▄████▄ ▀████

█▓▒▒░░░ version 1.0.0░░░▒▒▓█
```


## Use

```bash
git clone git@gitlab.com:mlc-labs/dev-kit.git
cd dev-kit
source dev-kit.sh
```

after that, you can check the use executing the command:

```bash
<$ dev-kit

Usage: dev-kit <tool> <command>

tools:
    dev-base
    docker
    editor
    git
    java
    latex
    node
    python

commands:
 install : install the tools

<$
```

## Other specials commands

```
dev-kit git promt     # install a promt to show  git information
dev-kit java tools    # install maven, gradle, visualvm, etc...
dev-kit java ide      # install intellj ide
dev-kit node tools    # install yarn, vue-cli
dev-kit python tools  # install pipenv
```

## Resources

- https://github.com/alebcay/awesome-shell
- https://github.com/awesome-lists/awesome-bash

## License

This project is published under the term of the [MIT](./LICENSE).
