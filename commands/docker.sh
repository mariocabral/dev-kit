#!/usr/bin/env bash
source utils/log4bash.sh
source utils/os.sh

__ubuntu_cmd__install(){
  log_info "Installing docker ce...";
  local packages="docker-ce"
  sudo apt -y install apt-transport-https ca-certificates software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
  sudo apt update
  sudo apt -y install ${packages}
  ## https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user
  log_success "updating docker user";
  sudo groupadd docker
  sudo usermod -aG docker $USER
  docker run hello-world
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
  #
  # Docker Compose
  #
  log_info "Installing docker-compose...";
  sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  log_success "Instalation ${YELLOW}docker-compose${LOG_SUCCESS_COLOR} complete";
}

__arch_cmd__install(){
  log_info "Installing docker ce...";
  sudo pacman -Syu
  sudo pacman -S docker
  sudo systemctl start docker.service
  sudo systemctl enable docker.service
  sudo docker version
  
  log_success "updating docker user";
  sudo usermod -aG docker $USER
  docker run hello-world
  log_success "Instalation ${YELLOW}docker${LOG_SUCCESS_COLOR} complete";
  #
  # Docker Compose
  #
  log_info "Installing docker-compose...";
  sudo pacman -S docker-compose
  log_success "Instalation ${YELLOW}docker-compose${LOG_SUCCESS_COLOR} complete";
  log_warning "To complete the instalation, restart the computer"
}

cmd__install(){
  __check_distro
  if [ $CURRENT_DISTRO == $ARCH_DISTRO_ID ]
  then
    __arch_cmd__install
  elif [ $CURRENT_DISTRO == $UBUNTU_DISTRO_ID ]
  then
    __ubuntu_cmd__install
  fi
}