#!/usr/bin/env bash

source utils/log4bash.sh

cmd__install(){
  log_info "Prepare installations...";
  log_info "Installing sdk man...";
  curl -s "https://get.sdkman.io" | bash
  source "$HOME/.sdkman/bin/sdkman-init.sh"
  log_info "Installing java...";
  sdk install java 14.0.2.j9-adpt
  sdk install java 11.0.9.j9-adpt
  sdk install java 8.0.292.j9-adpt
  log_success "Instalation ${YELLOW}java${LOG_SUCCESS_COLOR} complete";
}

cmd__tools(){
  source "$HOME/.sdkman/bin/sdkman-init.sh"
  log_info "Installing maven...";
  sdk install maven
  log_info "Installing gradle...";
  sdk install gradle
  log_info "Installing groovy...";
  sdk install groovy
  log_info "Installing springboot...";
  sdk install springboot
  log_info "Installing visualvm...";
  sdk install visualvm
  log_success "Instalation ${YELLOW} gradle, groovy, springboot and visualvm${LOG_SUCCESS_COLOR} complete";
}


cmd__ide(){
  log_info "Installing jetbrains ideaIC...";
  wget -O /tmp/ideaIC.tar.gz https://download.jetbrains.com/idea/ideaIC-2021.2.tar.gz
  sudo mkdir /opt/ideaIC
  sudo chown $USER:$USER /opt/ideaIC
  tar -C /opt/ideaIC -vxzf /tmp/ideaIC.tar.gz
  sh /opt/ideaIC/*/bin/idea.sh &
}
