#!/usr/bin/env bash

source utils/log4bash.sh


cmd__promt(){
  log_info "Installing magicmonty/bash-git-prompt...";
  git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

  if grep -q bash-git-prompt "$HOME/.bashrc"; then
    log_debug "git prompt already integrate to Bash "
  else
    log_debug "updating file $HOME/.bashrc "
    echo " " >> $HOME/.bashrc
    echo "# https://github.com/magicmonty/bash-git-prompt.git " >> $HOME/.bashrc
    echo "if [ -f \"\$HOME/.bash-git-prompt/gitprompt.sh\" ]; then" >> $HOME/.bashrc
    echo "  GIT_PROMPT_ONLY_IN_REPO=1" >> $HOME/.bashrc
    echo "  source $HOME/.bash-git-prompt/gitprompt.sh" >> $HOME/.bashrc
    echo "fi" >> $HOME/.bashrc
  fi
  log_success "Instalation ${YELLOW}magicmonty/bash-git-prompt${LOG_SUCCESS_COLOR} complete";
}


cmd__config(){
  log_info "Updating configuration of gitlab...";
  
  git config --global user.name "Mario Cabral"
  git config --global user.email mlc-lab@protonmail.com
  
  git config --global color.ui auto

  git config --global alias.ci commit
  git config --global alias.cim  "commit -m"
  git config --global alias.co checkout
  git config --global alias.st status
  git config --global alias.br branch
  git config --global alias.d diff
  git config --global alias.amend "ci --amend"
  git config --global alias.pull "pull --ff-only"
  git config --global alias.log "-10 --color=always --all --graph --topo-order --pretty='%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"

  log_success "Configuration ${YELLOW}git${LOG_SUCCESS_COLOR} complete";
}