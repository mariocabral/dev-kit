#!/usr/bin/env bash


source utils/log4bash.sh
source utils/os.sh

__ubuntu_cmd__install(){
  log_info "Installing packages...";
  local packages="git make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget \
                  curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
                  default-libmysqlclient-dev"
  sudo apt install --no-install-recommends ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}

__arch_cmd__install(){
  log_info "Updating system...";
  sudo pacman-mirrors --fasttrack
  sudo pacman-db-upgrade && sync
  sudo pacman -Syu
  local packages="git unzip zip make wget curl tree htop"
  sudo pacman -S ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}


cmd__install(){
  __check_distro
  if [ $CURRENT_DISTRO == $ARCH_DISTRO_ID ]
  then
    __arch_cmd__install
  elif [ $CURRENT_DISTRO == $UBUNTU_DISTRO_ID ]
  then
    __ubuntu_cmd__install
  fi
}
