#!/usr/bin/env bash

source utils/log4bash.sh

cmd__install(){
  log_info "Prepare installations...";
  log_info "Installing nvm...";
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
  source "$HOME/.nvm/nvm.sh"
  log_info "Installing node...";
  nvm install v12.18.3
  nvm install v10.22.0
  nvm install v8.17.0
  log_success "Instalation ${YELLOW}nvm, node (v12.18.3, v10.22.0, v8.17.0), yarn, @vue/cli${LOG_SUCCESS_COLOR} complete";
}

cmd__tools(){
  log_info "Installing yarn...";
  nvm use v12.18.3
  npm install -g yarn
  nvm use v10.22.0
  npm install -g yarn
  nvm use v8.17.0
  npm install -g yarn
  log_info "Installing @vue/cli...";
  nvm use v12.18.3
  yarn global add @vue/cli
  nvm use v10.22.0
  yarn global add @vue/cli
  log_success "Instalation ${YELLOW} yarn, @vue/cli${LOG_SUCCESS_COLOR} complete";
}
