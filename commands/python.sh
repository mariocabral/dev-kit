#!/usr/bin/env bash

source utils/log4bash.sh
source utils/os.sh

__ubuntu_cmd__install(){
  log_info "Installing python ...";
  local packages="python3.8 python3.8-dev python3.9 python3.9-dev python3-pip"
  sudo apt install ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}

__arch_cmd__install(){
  log_info "Installing python ...";
  local packages="python-pip"
  sudo pacman -S ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}

cmd__install(){
  __check_distro
  if [ $CURRENT_DISTRO == $ARCH_DISTRO_ID ]
  then
    __arch_cmd__install
  elif [ $CURRENT_DISTRO == $UBUNTU_DISTRO_ID ]
  then
    __ubuntu_cmd__install
  fi
}

cmd__tools(){
  log_info "Installing python tools ...";
  log_info "Installing pipenv...";
  pip3 install --user pipenv
  local line="/home/john/.local/bin"
  if grep -q "$line" "$HOME/.bashrc"; then
    log_debug "local bin already integrate to user PATH "
  else
    log_debug "updating file $HOME/.bashrc "
    echo "" >> $HOME/.bashrc
    echo "# Adding line to update PATH" >> $HOME/.bashrc
    echo "export PATH=\"\$PATH:/home/john/.local/bin\""  >> $HOME/.bashrc
    echo "" >> $HOME/.bashrc
  fi
  log_success "Instalation ${YELLOW}pipenv${LOG_SUCCESS_COLOR} complete";
}
