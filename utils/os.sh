#!/usr/bin/env bash

source utils/log4bash.sh


ARCH_DISTRO_ID=1
UBUNTU_DISTRO_ID=2
CURRENT_DISTRO=$UBUNTU_DISTRO_ID

function __check_distro(){
    local OS=$(awk '/DISTRIB_ID=/' /etc/*-release | sed 's/DISTRIB_ID=//' | tr '[:upper:]' '[:lower:]')
    local ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
    local VERSION=$(awk '/DISTRIB_RELEASE=/' /etc/*-release | sed 's/DISTRIB_RELEASE=//' | sed 's/[.]0/./')

    if [ -z "$OS" ]; then
        OS=$(awk '{print $1}' /etc/*-release | tr '[:upper:]' '[:lower:]')
    fi

    if [ -z "$VERSION" ]; then
        VERSION=$(awk '{print $3}' /etc/*-release)
    fi
    
    log_info "Linux distro detected: "
    log_info "      OS: $OS"
    log_info "    ARCH: $ARCH"
    log_info " VERSION: $VERSION"
    echo " "
    if [ $OS == "manjarolinux" ]
    then
        CURRENT_DISTRO=$ARCH_DISTRO_ID
    elif [ $OS == "neon" ]
    then
        CURRENT_DISTRO=$UBUNTU_DISTRO_ID
    elif [ $OS == "ubuntu" ]
    then
        CURRENT_DISTRO=$UBUNTU_DISTRO_ID
    else
        log_error "Unsupported Operating System"
        exit 1
    fi
}
