##!/usr/bin/env bash

source utils/version.sh

__banner (){
  echo " "
  echo "        ▄▄                                       ▄▄"
  echo "      ▀███                           ▀███        ██   ██"
  echo "        ██                             ██             ██"
  echo "   ▄█▀▀███   ▄▄█▀██▀██▀   ▀██▀         ██  ▄██▀▀███ ██████"
  echo " ▄██    ██  ▄█▀   ██ ██   ▄█           ██ ▄█     ██   ██"
  echo " ███    ██  ██▀▀▀▀▀▀  ██ ▄█     █████  ██▄██     ██   ██"
  echo " ▀██    ██  ██▄    ▄   ███             ██ ▀██▄   ██   ██"
  echo "  ▀████▀███▄ ▀█████▀    █            ▄████▄ ██▄▄████▄ ▀████"
  echo " "
  echo "     █▓▒▒░░░ version ${DEV_KIT_VERSION}░░░▒▒▓█"
}


__devkit_help (){
  echo ""
  echo "Usage: dev-kit <tool> <command>"
  echo ""
  echo "tools:"
  local search_dir="./commands"
  for entry in "$search_dir"/*; do
    local filename=$(basename -- "$entry")
    echo "    ${filename%.*}"
  done
  echo ""
  echo "commands:"
  echo " install : install the tools"
  echo ""
}
