#!/usr/bin/env bash
source utils/log4bash.sh
source utils/help.sh

dev-kit() {
  if [ $# -eq 0 ]; then
    __devkit_help
  else
    __banner
    local tool=$1
    if [ ! -f "commands/${tool}.sh" ]; then
      echo ""
      log_error "this ${YELLOW}tool: ${tool}${LOG_ERROR_COLOR} does not integrated yet."
      __devkit_help
    else
      source commands/${tool}.sh
      local task=$2
      log_debug "executing ${YELLOW}${task}${LOG_DEBUG_COLOR} from ${YELLOW}commands/${tool}.sh${LOG_DEBUG_COLOR}";

      if [ "$(type -t cmd__${task})" == 'function' ]; then
        cmd__${task}
      else
        echo ""
        log_error "this ${YELLOW}command: ${task}${LOG_ERROR_COLOR} does not implemented yet."
        __devkit_help
      fi
    fi
  fi
}
